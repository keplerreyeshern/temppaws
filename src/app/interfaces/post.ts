import { User } from "./user";

export interface Post {
  _id: string;
  created: Date;
  message: string;
  images: string[];
  coords: string;
  user: User;
}
