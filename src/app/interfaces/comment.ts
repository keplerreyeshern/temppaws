import { User } from "./user";
import { Post } from "./post";

export interface Comment {
  _id: string;
  created: Date;
  message: string;
  images: string[];
  post: Post;
  user: User;
}
