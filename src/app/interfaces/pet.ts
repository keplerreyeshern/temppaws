import { User } from "./user";

export interface Pet{
  _id: string;
  created: Date;
  name: string;
  species: string;
  status: string;
  statusDetails: string;
  gender: string;
  breed: string;
  tattooCode: string;
  coatLength: string;
  primaryColor: string;
  secondaryColor: string;
  tertiaryColor: string;
  age: number
  size: string;
  weight: number;
  sterilized: boolean;
  microchipMark: string;
  microchipID: string;
  implantDate: Date;
  images: string[];
  user: User;
}
