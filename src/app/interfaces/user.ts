export interface User{
  _id: string;
  slug: string;
  name: string;
  email:string;
  password: string;
  avatar: string;
  profile: string;
  active: boolean;
}
