import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { environment } from "../../../environments/environment";
import { StorageService } from "../storage/storage.service";

@Injectable({
  providedIn: 'root'
})
export class LikesService {

  baseUrl = environment.baseUrl + '/likes';
  headers: any;
  token: string = '';

  constructor(private http: HttpClient,
              private storage: StorageService) { }


  postLike(params: any){
    this.token = this.storage.getToken();
    let headers = new HttpHeaders({
      'x-token': this.token,
    });
    return this.http.post<any>(this.baseUrl, params, {headers});
  }

  getLikes(postId: string){
    return this.http.get<any>(this.baseUrl + '/' + postId);
  }

  getLike(postId: string){
    this.token = this.storage.getToken();
    let headers = new HttpHeaders({
      'x-token': this.token,
    });
    return this.http.get<any>(this.baseUrl + '/like/' + postId, {headers});
  }
}
