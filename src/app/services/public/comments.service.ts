import { Injectable } from '@angular/core';
import {environment} from "../../../environments/environment";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {StorageService} from "../storage/storage.service";

@Injectable({
  providedIn: 'root'
})
export class CommentsService {

  baseUrl = environment.baseUrl + '/comments';
  headers: any;
  token: string = '';

  constructor(private http: HttpClient,
              private storage: StorageService) { }


  postComment(params: any){
    this.token = this.storage.getToken();
    let headers = new HttpHeaders({
      'x-token': this.token,
    });
    return this.http.post<any>(this.baseUrl , params, {headers});
  }

  getComments(postId: string){
    this.token = this.storage.getToken();
    let headers = new HttpHeaders({
      'x-token': this.token,
    });
    return this.http.get<any>(this.baseUrl + '/' + postId, {headers});
  }
}
