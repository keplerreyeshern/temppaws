import { Injectable } from '@angular/core';
import {environment} from "../../../environments/environment";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {StorageService} from "../storage/storage.service";

@Injectable({
  providedIn: 'root'
})
export class PostsService {

  baseUrl = environment.baseUrl;
  headers: any;
  token: string = '';

  constructor(private http: HttpClient,
              private storage: StorageService) {

  }

  getPosts(page: number){
    let pages = '';
    if (page != 0){
      pages = '?page=' + page;
    }
    this.token = this.storage.getToken();
    let headers = new HttpHeaders({
      'x-token': this.token,
    });
    return this.http.get<any>(this.baseUrl + '/posts' + pages, {headers});
  }

  getPostsUser(userId: string){
    this.token = this.storage.getToken();
    let headers = new HttpHeaders({
      'x-token': this.token,
    });
    return this.http.get<any>(this.baseUrl + '/posts/' + userId, {headers});
  }

  postPost(params: any){
    this.token = this.storage.getToken();
    let headers = new HttpHeaders({
      'x-token': this.token,
    });
    return this.http.post<any>(this.baseUrl + '/posts', params,{headers});
  }

  postImage(params: any){
    this.token = this.storage.getToken();
    let headers = new HttpHeaders({
      'x-token': this.token,
    });
    return this.http.post<any>(this.baseUrl + '/posts/upload', params,{headers});
  }
}
