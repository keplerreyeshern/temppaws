import { Injectable } from '@angular/core';
import {environment} from "../../../environments/environment";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {StorageService} from "../storage/storage.service";

@Injectable({
  providedIn: 'root'
})
export class PetsService {

  baseUrl = environment.baseUrl + '/pets';
  headers: any;
  token: string = '';

  constructor(private http: HttpClient,
              private storage: StorageService) {

  }

  getPets(page: number){
    let pages = '';
    if (page != 0){
      pages = '?page=' + page;
    }
    this.token = this.storage.getToken();
    let headers = new HttpHeaders({
      'x-token': this.token,
    });
    return this.http.get<any>(this.baseUrl + pages, {headers});
  }

  getPetUser(userId: string){
    this.token = this.storage.getToken();
    let headers = new HttpHeaders({
      'x-token': this.token,
    });
    return this.http.get<any>(this.baseUrl + '/' + userId, {headers});
  }

  postPet(params: any){
    this.token = this.storage.getToken();
    let headers = new HttpHeaders({
      'x-token': this.token,
    });
    return this.http.post<any>(this.baseUrl, params,{headers});
  }

  postImage(params: any){
    this.token = this.storage.getToken();
    let headers = new HttpHeaders({
      'x-token': this.token,
    });
    return this.http.post<any>(this.baseUrl + '/upload', params,{headers});
  }
}
