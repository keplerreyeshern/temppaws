import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class NavbarService {

  public isLoggedIn = false;

  constructor() { }

  signOut(){
    this.isLoggedIn = false;
    localStorage.setItem('isLoggedIn', 'false')
  }

  signIn(){
    this.isLoggedIn = true;
    localStorage.setItem('isLoggedIn', 'true')
  }

  getIsLoggedIn(): boolean{
    if (!localStorage.getItem('isLoggedIn')){
      return this.isLoggedIn;
    } else {
      return  this.getBoolean(<string>localStorage.getItem('isLoggedIn'));
    }
  }

  getBoolean(option: string): boolean{
    if (option == 'true'){
      return true;
    } else {
      return false;
    }
  }
}
