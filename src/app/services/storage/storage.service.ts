import { Injectable } from '@angular/core';
import {environment} from "../../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {User} from "../../interfaces/user";
import {json} from "express";

@Injectable({
  providedIn: 'root'
})
export class StorageService {

  baseUrl = environment.baseUrl + '/api/password';
  token: string = '';
  user:User=<User>{};

  constructor(private http: HttpClient) {

  }

  setToken(token: string){
    this.token = token;
    localStorage.setItem('token', token);
  }

  getToken(): string{
    if(!localStorage.getItem('token')){
      if (this.token == ''){
        return '';
      } else {
        return this.token;
      }
    } else {
      return <string>localStorage.getItem('token');
    }
  }

  setUser(user: User){
    user.active = true;
    this.user = user;
    localStorage.setItem('user', JSON.stringify(user));
  }

  getUser(): User{
    if (Object.keys(this.user).length === 0){
      return JSON.parse(<string>localStorage.getItem('user'));
    } else {
      return this.user;
    }

  }

  signOut(){
    this.token = '';
    localStorage.clear();
  }
}
