import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PublicRoutingModule } from './public-routing.module';
import { PublicComponent } from './public.component';
import { PublicComponentsModule } from "../../components/public/public-components.module";
import { NgxSpinnerModule } from "ngx-spinner";


@NgModule({
  declarations: [
    PublicComponent
  ],
  imports: [
    CommonModule,
    PublicRoutingModule,
    PublicComponentsModule,
    NgxSpinnerModule
  ]
})
export class PublicModule { }
