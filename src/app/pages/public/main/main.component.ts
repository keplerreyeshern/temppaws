import { Component, OnInit } from '@angular/core';
import { PetsService } from "../../../services/public/pets.service";
import { NgxSpinnerService } from "ngx-spinner";
import { ToastService } from "../../../services/components/toast.service";

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.sass']
})
export class MainComponent implements OnInit {

  pets:any[]=[];
  private actualPage: number = 1;

  constructor(private petsService: PetsService,
              private loading: NgxSpinnerService,
              private toastService: ToastService) { }

  ngOnInit(): void {
    this.getData(0);
  }

  publishPost(e: string){
    this.showSuccess(e);
    this.getData(1);
  }

  onScroll() {
    this.actualPage++;
    console.log(this.actualPage);
    this.getData(this.actualPage);
  }

  getData(page: number){
    this.loading.show();
    this.petsService.getPets(page).subscribe(response => {
      if (response.ok){
        for(let i=0;response.pets.length>i;i++){
          this.pets.push(response.pets[i]);
        }
      }
      this.loading.hide();
    }, error => {
      if (error.status == 500){
        this.showDanger('Se encontro un error en el servidor, comunicate con el Administrador del web site');
      } else {
        this.showDanger('Se encontro un error, comunicate con el Administrador del web site');
      }
      console.log(error);
      this.loading.hide();
    });
  }

  showSuccess(message: string) {
    this.toastService.show(message, { classname: 'bg-success text-light', delay: 10000 });
  }

  showDanger(message: any) {
    this.toastService.show(message, { classname: 'bg-danger text-light', delay: 15000 });
  }

}
