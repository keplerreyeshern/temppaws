import { Component, OnInit } from '@angular/core';
import { User } from "../../../interfaces/user";
import { Alert } from "../../../interfaces/alert";
import { NgxSpinnerService } from "ngx-spinner";
import { AuthService } from "../../../services/sign/auth.service";
import { StorageService } from "../../../services/storage/storage.service";
import { Router } from "@angular/router";
import { NgForm } from "@angular/forms";
import { faEye, faEyeSlash } from "@fortawesome/free-regular-svg-icons";
import { response } from "express";
import { NavbarService } from "../../../services/components/navbar.service";
import { DataService } from "../../../services/data/data.service";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.sass']
})
export class RegisterComponent implements OnInit {

  user: User=<User>{};
  alert:Alert = <Alert>{};
  type = 'password';
  faPassword = faEye;

  constructor(private loading: NgxSpinnerService,
              private authService: AuthService,
              private dataService: DataService,
              private storage: StorageService,
              private componentNavbar: NavbarService,
              private router: Router) {}

  ngOnInit(): void {
    this.alert.type = 'danger';
  }

  submit(form: NgForm){
    if (form.value.password != form.value.passwordRepeat){
      this.alert = {
        type: 'danger',
        message: 'Las contraseñas deben coincidir',
        active: true
      };
    } else if (form.value.password < 7){
      this.alert = {
        type: 'danger',
        message: 'Las contraseñas deben tener minimo 8 caracteres',
        active: true
      };
    } else {
      this.loading.show();
      let params = new FormData();
      params.append('name', form.value.name);
      params.append('email', form.value.email);
      params.append('password', form.value.password);
      params.append('profile', 'user');
      this.authService.register(params).subscribe(response => {
        console.log(response);
        if (response.ok) {
          console.log(response.token);
          this.authService.signIn();
          this.storage.setToken(response.token);
          this.componentNavbar.signIn();
          this.getUser(response.token);
        } else {
          if (response.err.code == 11000) {
            this.alert = {
              type: 'danger',
              active: true,
              message: 'El email ' + response.err.keyValue.email + ' ya se encuentra en nuestra base de datos. Intenta con otro'
            }
          }
        }
        this.loading.hide();
      }, error => {
        if (error.status == 500) {
          this.alert = {
            type: 'danger',
            active: true,
            message: 'Se encontro un error en el servidor, comunicate con el Administrador del web site'
          }
        } else {
          this.alert = {
            type: 'danger',
            active: true,
            message: 'Se encontro un error, comunicate con el Administrador del web site'
          }
        }
        this.loading.hide();
      });
    }
  }

  getUser(token: string){
    this.dataService.getUser(token).subscribe(response => {
      if (response.ok){
        this.storage.setUser(response.user);
        this.router.navigateByUrl('/sign/registra/mascota');
      } else {
        this.alert = {
          type: 'danger',
          active: true,
          message: 'Se encontro un error en el servidor, comunicate con el Administrador del web site'
        }
      }
    }, error => {
      if (error.status == 500){
        this.alert = {
          type: 'danger',
          active: true,
          message: 'Se encontro un error en el servidor, comunicate con el Administrador del web site'
        }
      } else {
        this.alert = {
          type: 'danger',
          active: true,
          message: 'Se encontro un error, comunicate con el Administrador del web site'
        }
      }
      console.log(error);
      this.loading.hide();
    });
  }

  togglePassword(){
    if (this.type == 'password'){
      this.type = 'text';
      this.faPassword = faEyeSlash;
    } else {
      this.type = 'password';
      this.faPassword = faEye;
    }
  }

  setTime(){
    setTimeout(() => {
      this.alert.active = false;
    },10000);
  }

  closeAlert() {
    this.alert.active = false;
  }

}
