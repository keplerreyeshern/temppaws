import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SignComponent } from './sign.component';
import { AuthGuard } from "../../guards/auth.guard";

const routes: Routes = [
  {
    path: '',
    component: SignComponent,
    children: [
      {
        path: 'entrar',
        loadChildren: () => import('./login/login.module').then(m => m.LoginModule)
      },
      {
        path: 'registrate',
        loadChildren: () => import('./register/register.module').then(m => m.RegisterModule)
      },
      {
        path: 'registra/mascota',
        canActivate: [AuthGuard],
        loadChildren: () => import('./register-pet/register-pet.module').then(m => m.RegisterPetModule)
      },
      {
        path: '',
        redirectTo: 'entrar'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SignRoutingModule { }
