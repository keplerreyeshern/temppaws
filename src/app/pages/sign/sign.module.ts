import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SignRoutingModule } from './sign-routing.module';
import { SignComponent } from './sign.component';
import { LoginModule } from "./login/login.module";
import { RegisterModule } from "./register/register.module";
import { PublicComponentsModule } from "../../components/public/public-components.module";


@NgModule({
  declarations: [
    SignComponent
  ],
  imports: [
    CommonModule,
    SignRoutingModule,
    LoginModule,
    RegisterModule,
    PublicComponentsModule
  ]
})
export class SignModule { }
