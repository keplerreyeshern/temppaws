import { Component, OnInit } from '@angular/core';
import { AuthService } from "../../../services/sign/auth.service";
import { NgxSpinnerService } from "ngx-spinner";
import { Alert } from "../../../interfaces/alert";
import { Router } from "@angular/router";
import { NgForm } from "@angular/forms";
import { User } from "../../../interfaces/user";
import { StorageService } from "../../../services/storage/storage.service";
import { faEye, faEyeSlash } from "@fortawesome/free-regular-svg-icons";
import { NavbarService } from "../../../services/components/navbar.service";
import { DataService } from "../../../services/data/data.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.sass']
})
export class LoginComponent implements OnInit {

  user: User=<User>{};
  alert:Alert = <Alert>{};
  type = 'password';
  faPassword = faEye;

  constructor(private loading: NgxSpinnerService,
              private authService: AuthService,
              private componentNavbar: NavbarService,
              private dataService: DataService,
              private storage: StorageService,
              private router: Router) {}

  ngOnInit(): void {
    this.alert.type = 'danger';
  }

  submit(form: NgForm){
    this.loading.show();
    let params = {
      email: form.value.email,
      password: form.value.password
    }
    this.authService.login(params).subscribe( response => {
      if (response.ok){
        this.authService.signIn();
        this.storage.setToken(response.token);
        this.componentNavbar.signIn();
        this.getUser(response.token);
      } else {
        this.alert = {
          type: 'danger',
          active: true,
          message: response.message
        }
      }
      this.loading.hide();
    }, error => {
      if (error.status == 500){
        this.alert = {
          type: 'danger',
          active: true,
          message: 'Se encontro un error en el servidor, comunicate con el Administrador del web site'
        }
      } else {
        this.alert = {
          type: 'danger',
          active: true,
          message: 'Se encontro un error, comunicate con el Administrador del web site'
        }
      }
      this.loading.hide();
    });

  }

  getUser(token: string){
    this.dataService.getUser(token).subscribe(response => {
      if (response.ok){
        this.storage.setUser(response.user);
        this.router.navigateByUrl('/');
      } else {
        this.alert = {
          type: 'danger',
          active: true,
          message: 'Se encontro un error en el servidor, comunicate con el Administrador del web site'
        }
      }
    }, error => {
      if (error.status == 500){
        this.alert = {
          type: 'danger',
          active: true,
          message: 'Se encontro un error en el servidor, comunicate con el Administrador del web site'
        }
      } else {
        this.alert = {
          type: 'danger',
          active: true,
          message: 'Se encontro un error, comunicate con el Administrador del web site'
        }
      }
      console.log(error);
      this.loading.hide();
    });
  }

  togglePassword(){
    if (this.type == 'password'){
      this.type = 'text';
      this.faPassword = faEyeSlash;
    } else {
      this.type = 'password';
      this.faPassword = faEye;
    }
  }

  setTime(){
    setTimeout(() => {
      this.alert.active = false;
    },10000);
  }

  closeAlert() {
    this.alert.active = false;
  }

}
