import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RegisterPetComponent } from './register-pet.component';

const routes: Routes = [{ path: '', component: RegisterPetComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RegisterPetRoutingModule { }
