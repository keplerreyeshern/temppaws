import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RegisterPetRoutingModule } from './register-pet-routing.module';
import { RegisterPetComponent } from './register-pet.component';
import { FontAwesomeModule } from "@fortawesome/angular-fontawesome";
import { FormsModule } from "@angular/forms";


@NgModule({
  declarations: [
    RegisterPetComponent
  ],
  imports: [
    CommonModule,
    RegisterPetRoutingModule,
    FontAwesomeModule,
    FormsModule
  ]
})
export class RegisterPetModule { }
