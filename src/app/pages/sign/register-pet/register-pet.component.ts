import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from "ngx-spinner";
import { PetsService } from "../../../services/public/pets.service";
import { ToastService } from "../../../services/components/toast.service";
import { NgForm } from "@angular/forms";
import { Router } from "@angular/router";

@Component({
  selector: 'app-register-pet',
  templateUrl: './register-pet.component.html',
  styleUrls: ['./register-pet.component.sass']
})
export class RegisterPetComponent implements OnInit {


  constructor(private loading: NgxSpinnerService,
              private service: PetsService,
              private router: Router,
              private toastService: ToastService) { }

  ngOnInit(): void {
  }

  registerPet(form: NgForm){
    this.loading.show()
      let params = new FormData();
      params.append('Content-Type', 'multipart/form-data');
      params.append('name', form.value.name);
      params.append('species', form.value.species);
      params.append('status', form.value.status);
      params.append('statusDetails', form.value.statusDetails);
      params.append('gender', form.value.gender);
      params.append('breed', form.value.breed);
      params.append('tattooCode', form.value.tattooCode);
      params.append('coatLength', form.value.coatLength);
      params.append('primaryColor', form.value.primaryColor);
      params.append('secondaryColor', form.value.secondaryColor);
      params.append('tertiaryColor', form.value.tertiaryColor);
      params.append('age', form.value.age);
      params.append('size', form.value.size);
      params.append('weight', form.value.weight);
      if (form.value.sterilized) {
        params.append('sterilized', 'true');
      }
      params.append('microchipMark', form.value.microchipMark);
      params.append('microchipID', form.value.microchipID);
      params.append('implantDate', form.value.implantDate);
      this.service.postPet(params).subscribe(response => {
        this.router.navigateByUrl('/');
        this.showSuccess('Tu perfil y tu mascota se registraron con exito');
        this.loading.hide();
      }, error => {
        this.showDanger('Se encontro un error en el servidor, comunicate con el administrador del web-site');
        this.loading.hide();
      });
  }


  showSuccess(message: string) {
    this.toastService.show(message, { classname: 'bg-success text-light', delay: 10000 });
  }

  showDanger(message: any) {
    this.toastService.show(message, { classname: 'bg-danger text-light', delay: 15000 });
  }


}
