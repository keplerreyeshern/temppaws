import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { faHome, faUsers, faUser, faEllipsisV, faCog, faSignOutAlt } from "@fortawesome/free-solid-svg-icons";
import { User } from "../../../interfaces/user";
import { AuthService } from "../../../services/sign/auth.service";
import { Alert} from "../../../interfaces/alert";
import { NgxSpinnerService } from "ngx-spinner";
import { StorageService } from "../../../services/storage/storage.service";
import { NavbarService } from "../../../services/components/navbar.service";

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.sass']
})
export class NavbarComponent implements OnInit {

  faUser = faUser;
  faEllipsisV = faEllipsisV;
  faCog = faCog;
  faUsers = faUsers;
  faHome = faHome;
  faSignOutAlt = faSignOutAlt;
  open = false;
  display = 'none';
  user:User=<User>{};
  alert:Alert=<Alert>{};

  constructor(private router: Router,
              private loading: NgxSpinnerService,
              private storage: StorageService,
              private serviceAuth: AuthService,
              public component: NavbarService) { }

  ngOnInit(): void {
    this.getData();
  }

  getData(){
    this.user = this.storage.getUser();
  }

  toogleSub(){
    if (this.display == 'none'){
      this.display = 'block';
    } else {
      this.display = 'none';
    }
  }

  show(){
    this.display = 'block';
  }

  hide(){
    this.display = 'none';
  }

  toogle(){
    if (this.open){
      this.open = false;
    } else {
      this.open = true;
    }
    // this.componentService.setOpen(this.open);
  }

  sigout(){
    this.serviceAuth.signOut();
    this.storage.signOut();
    this.user = <User>{};
    this.component.signOut();
    // this.router.navigate(['/main']);
  }

}
