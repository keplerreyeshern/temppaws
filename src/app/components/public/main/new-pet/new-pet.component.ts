import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { User } from "../../../../interfaces/user";
import { Alert } from "../../../../interfaces/alert";
import { Router } from "@angular/router";
import { NgxSpinnerService } from "ngx-spinner";
import { AuthService } from "../../../../services/sign/auth.service";
import { DataService } from "../../../../services/data/data.service";
import { faFileImage, faImages } from "@fortawesome/free-regular-svg-icons";
import { faTimes } from "@fortawesome/free-solid-svg-icons";
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { PetsService } from "../../../../services/public/pets.service";
import { StorageService } from "../../../../services/storage/storage.service";
import { NavbarService } from "../../../../services/components/navbar.service";
import { Pet } from "../../../../interfaces/pet";
import { ToastService } from "../../../../services/components/toast.service";

@Component({
  selector: 'app-new-pet',
  templateUrl: './new-pet.component.html',
  styleUrls: ['./new-pet.component.sass']
})
export class NewPetComponent implements OnInit {

  @Output() publishPost = new EventEmitter<string>();

  user:User=<User>{
    name: '',
  };
  userName: string = '';
  alert:Alert=<Alert>{};
  faFileImage = faFileImage;
  faTimes = faTimes;
  faImages = faImages;
  files: File[] = [];
  filesLength = 0;
  filesLengthCount = 0;
  message:string = '';
  pet:Pet=<Pet>{
    name: ''
  };
  date:string= '';

  constructor(private router: Router,
              private loading: NgxSpinnerService,
              private storage: StorageService,
              private toastService: ToastService,
              private serviceAuth: AuthService,
              private servicePet: PetsService,
              public componetNavbar: NavbarService,
              private serviceData: DataService,
              private modalService: NgbModal) { }

  ngOnInit(): void {
    this.getData();
  }

  onSelect(event: any) {
    this.files.push(...event.addedFiles);
    console.log(this.files);
  }

  onRemove(event: any) {
    console.log(event);
    this.files.splice(this.files.indexOf(event), 1);
  }

  autoSize(item: any){
    item.style.cssText = 'height:auto; padding:0';
    item.style.cssText = 'height:' + item.scrollHeight + 'px';
  }

  getData(){
    this.loading.show();
    this.user =  this.storage.getUser();
    this.userName = this.user.name.substr(0, this.user.name.indexOf(' '));
    this.loading.hide();
  }

  openPhoto(content: any) {
    this.modalService.open(content, { centered: true });
  }

  closeModal() {
    this.modalService.dismissAll();
  }

  createPetPhoto(){
    this.filesLength = this.files.length;
    if (this.pet.age == undefined){
      this.showDanger('La edad es obligatoria');
    } else if(this.pet.weight == undefined){
      this.showDanger('El peso es obligatorio');
    } else {
      if (this.filesLength == 0) {
        this.createPet();
      } else {
        this.loading.show();
        for (let i = 0; this.files.length > i; i++) {
          let params = new FormData();
          params.append('Content-Type', 'multipart/form-data');
          params.append('image', this.files[i]);
          this.servicePet.postImage(params).subscribe(response => {
            console.log(response);
            if (response.ok) {
              this.filesLengthCount = this.filesLengthCount + 1;
              if (this.filesLength == this.filesLengthCount) {
                this.createPet();
              }
            } else {
              this.alert = {
                type: 'danger',
                active: true,
                message: response.message
              }
            }
          }, error => {
            if (error.status == 500) {
              this.alert = {
                type: 'danger',
                active: true,
                message: 'Se encontro un error en el servidor, comunicate con el Administrador del web site'
              }
            } else {
              this.alert = {
                type: 'danger',
                active: true,
                message: 'Se encontro un error, comunicate con el Administrador del web site'
              }
            }
            this.loading.hide();
            console.log(error);
          });
        }
      }
    }
  }

  createPet(){
    this.loading.show()
    if (this.filesLength == this.filesLengthCount){
      let params = new FormData();
      params.append('Content-Type', 'multipart/form-data');
      params.append('name', this.pet.name);
      params.append('species', this.pet.species);
      params.append('status', this.pet.status);
      params.append('statusDetails', this.pet.statusDetails);
      params.append('gender', this.pet.gender);
      params.append('breed', this.pet.breed);
      params.append('tattooCode', this.pet.tattooCode);
      params.append('coatLength', this.pet.coatLength);
      params.append('primaryColor', this.pet.primaryColor);
      params.append('secondaryColor', this.pet.secondaryColor);
      params.append('tertiaryColor', this.pet.tertiaryColor);
      params.append('age', this.pet.age.toString());
      params.append('size', this.pet.size);
      params.append('weight', this.pet.weight.toString());
      if (this.pet.sterilized) {
        params.append('sterilized', 'true');
      }
      params.append('microchipMark', this.pet.microchipMark);
      params.append('microchipID', this.pet.microchipID);
      params.append('implantDate', this.date);
      this.servicePet.postPet(params).subscribe(response => {
        this.message = '';
        this.publishPost.emit('!El post de ' + this.user.name + ' se publico con exito¡');
        this.showSuccess('La mascota se publico con exito');
        this.loading.hide();
        }, error => {
        this.showDanger('Se encontro un error en el servidor, comunicate con el administrador del web-site');
        this.loading.hide();
      });
      this.closeModal();
    }
  }


  showSuccess(message: string) {
    this.toastService.show(message, { classname: 'bg-success text-light', delay: 10000 });
  }

  showDanger(message: any) {
    this.toastService.show(message, { classname: 'bg-danger text-light', delay: 15000 });
  }

}
