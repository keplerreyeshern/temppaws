import { Component, Input, OnInit } from '@angular/core';
import { environment } from "../../../../../environments/environment";
import { User } from "../../../../interfaces/user";
import { Pet } from "../../../../interfaces/pet";
import { Gallery } from "angular-gallery";
import { StorageService } from "../../../../services/storage/storage.service";

@Component({
  selector: 'app-pets',
  templateUrl: './pets.component.html',
  styleUrls: ['./pets.component.sass']
})
export class PetsComponent implements OnInit {

  @Input() pets: Pet[]=[];
  baseUrl = environment.baseUrl;
  user:User=<User>{};

  constructor(public storage: StorageService,
              private gallery: Gallery) { }

  ngOnInit(): void {
  }

  showGallery(index: number, images: any, userId: string) {
    let imagesLoading: any[]=[];
    for (let i=0; images.length>i; i++){
      let image = {path: ''};
      image.path = this.baseUrl + '/pets/image/' + userId + '/' + images[i];
      imagesLoading.push(image);
    }
    let prop = {
      images: imagesLoading,
      index
    };
    this.gallery.load(prop);
  }

}
