import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SharedOptionsPetComponent } from './shared-options.component';

describe('SharedOptionsComponent', () => {
  let component: SharedOptionsPetComponent;
  let fixture: ComponentFixture<SharedOptionsPetComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SharedOptionsPetComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SharedOptionsPetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
