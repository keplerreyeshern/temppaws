import {Component, Input, OnInit, ViewChild} from '@angular/core';
import { faThumbsUp, faComment } from "@fortawesome/free-regular-svg-icons";
import { faThumbsUp as faUp } from "@fortawesome/free-solid-svg-icons";
import { faShareAlt, faTimes } from "@fortawesome/free-solid-svg-icons";
import { ToastService} from "../../../../services/components/toast.service";
import { NgxSpinnerService } from "ngx-spinner";
import { LikesService } from "../../../../services/public/likes.service";
import { environment } from "../../../../../environments/environment";
import { Like } from "../../../../interfaces/like";
import { User } from "../../../../interfaces/user";
import { StorageService} from "../../../../services/storage/storage.service";
import { CommentsComponent } from "../comments/comments.component";
import { Pet } from "../../../../interfaces/pet";

@Component({
  selector: 'app-shared-options-pet',
  templateUrl: './shared-options-pet.component.html',
  styleUrls: ['./shared-options-pet.component.sass']
})
export class SharedOptionsPetComponent implements OnInit {

  @Input() pet:Pet=<Pet>{};
  // @ts-ignore
  @ViewChild(CommentsComponent) comments: CommentsComponent;
  faComment = faComment;
  faShareAlt = faShareAlt;
  faThumbsUp = faThumbsUp;
  faUp = faUp;
  faTimes = faTimes;
  files: File[] = [];
  baseUrl = environment.baseUrl;
  like:Like=<Like>{};
  user:User=<User>{};
  likesLength = 0;
  commentsLength = 0;
  userName = '';

  constructor(private toastService: ToastService,
              private loading: NgxSpinnerService,
              public storage: StorageService,
              private likeServices: LikesService) { }

  ngOnInit(): void {
    this.user = this.storage.getUser();
    this.getLike();
  }

  postLike(postId: string){
    this.like.active = true;
    this.likesLength =  this.likesLength + 1;
    this.loading.show();
    const params = new FormData();
    params.append('post', postId);
    console.log(postId);
    this.likeServices.postLike(params).subscribe(response => {
      this.like = response.like;
      if (response.like.active){
        this.showSuccess('le diste like a el post de ' + this.pet.user.name);
      } else {
        this.showSuccess('quitaste tu like a el post de ' + this.pet.user.name);
      }
      this.loading.hide()
    }, error => {
      if (error.status == 500){
        this.showDanger('Se encontro un error en el servidor, comunicate con el Administrador del web site');
      } else {
        this.showDanger('Se encontro un error, comunicate con el Administrador del web site');
      }
    });
  }

  getLike(){
    this.loading.show();
    this.likeServices.getLike(this.pet._id).subscribe(response => {
      if (response.like != null){
        this.like = response.like;
      }
      this.getLikes();
      this.loading.hide();
    }, error => {
      if (error.status == 500){
        this.showDanger('Se encontro un error en el servidor, comunicate con el Administrador del web site');
      } else {
        this.showDanger('Se encontro un error, comunicate con el Administrador del web site');
      }
    });
  }

  commentsCount($event: any){
    this.commentsLength = $event;
  }

  getLikes(){
    this.loading.show();
    this.likeServices.getLikes(this.pet._id).subscribe(response => {
      this.likesLength = response.likes.length;
      if (response.likes.length > 0){
        this.userName = response.likes[0].user.name;
      }
      this.loading.hide();
    }, error => {
      if (error.status == 500){
        this.showDanger('Se encontro un error en el servidor, comunicate con el Administrador del web site');
      } else {
        this.showDanger('Se encontro un error, comunicate con el Administrador del web site');
      }
    });
  }


  showSuccess(message: string) {
    this.toastService.show(message, { classname: 'bg-success text-light', delay: 10000 });
  }

  showDanger(message: any) {
    this.toastService.show(message, { classname: 'bg-danger text-light', delay: 15000 });
  }

  focusInput(){
    this.comments.focusInput();
  }

}
