import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CommentsPetComponent } from './comments.component';

describe('CommentsComponent', () => {
  let component: CommentsPetComponent;
  let fixture: ComponentFixture<CommentsPetComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CommentsPetComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CommentsPetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
