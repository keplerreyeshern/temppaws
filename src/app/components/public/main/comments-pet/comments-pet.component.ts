import {Component, ElementRef, Input, OnInit, Output, ViewChild, EventEmitter} from '@angular/core';
import { Pet } from "../../../../interfaces/pet";
import { Comment } from "../../../../interfaces/comment";
import { CommentsService } from "../../../../services/public/comments.service";
import { ToastService } from "../../../../services/components/toast.service";
import { NgxSpinnerService } from "ngx-spinner";
import {StorageService} from "../../../../services/storage/storage.service";

@Component({
  selector: 'app-comments-pet',
  templateUrl: './comments-pet.component.html',
  styleUrls: ['./comments-pet.component.sass']
})
export class CommentsPetComponent implements OnInit {

  // @ts-ignore
  @ViewChild('commentInput') commentInput: ElementRef;
  @Input() pet:Pet=<Pet>{};
  @Output() commentsEvent = new EventEmitter<string>();
  comment:string='';
  comments:Comment[]=[];
  files: File[] = [];
  filesLength = 0;
  filesLengthCount = 0;

  constructor(private commentsService: CommentsService,
              public storage: StorageService,
              private toastService: ToastService,
              private loading: NgxSpinnerService) { }

  ngOnInit(): void {
    this.getData();
  }

  focusInput(){
    this.commentInput.nativeElement.focus();
  }

  getData(){
    this.loading.show();
    this.commentsService.getComments(this.pet._id).subscribe(response => {
      if (response.comments != null){
        this.comments = response.comments;
        this.commentsEvent.emit(response.comments.length);
      }
      this.loading.hide();
    }, error => {
      if (error.status == 500){
        this.showDanger('Se encontro un error en el servidor, comunicate con el Administrador del web site');
      } else {
        this.showDanger('Se encontro un error, comunicate con el Administrador del web site');
      }
    });
  }

  showSuccess(message: string) {
    this.toastService.show(message, { classname: 'bg-success text-light', delay: 10000 });
  }

  showDanger(message: any) {
    this.toastService.show(message, { classname: 'bg-danger text-light', delay: 15000 });
  }

  createCommentPhoto(){
    this.filesLength = this.files.length;
    if (this.filesLength == 0) {
      this.createComment();
    } else {
      // this.loading.show();
      // for (let i=0; this.files.length>i;i++){
      //   let params = new FormData();
      //   params.append('Content-Type', 'multipart/form-data');
      //   params.append('image', this.files[i]);
      //   this.commentsService.postImage(params).subscribe( response => {
      //     console.log(response);
      //     if (response.ok){
      //       this.filesLengthCount = this.filesLengthCount + 1;
      //       this.createPost();
      //     } else {
      //       this.showDanger(response.message);
      //     }
      //   }, error => {
      //     if (error.status == 500){
      //       this.showDanger('Se encontro un error en el servidor, comunicate con el Administrador del web site');
      //     } else {
      //       this.showDanger('Se encontro un error, comunicate con el Administrador del web site');
      //     }
      //     console.log(error);
      //     this.loading.hide();
      //   });
      // }
    }
  }


  createComment(){
    this.loading.show()
    if (this.filesLength == this.filesLengthCount){
      let params = new FormData();
      params.append('Content-Type', 'multipart/form-data');
      params.append('message', this.comment);
      params.append('post', this.pet._id);
      this.commentsService.postComment(params).subscribe(response => {
        console.log(response);
        this.getData();
        this.showSuccess('Se creo con exito el comentario');
        this.loading.hide();
      }, error => {
        if (error.status == 500){
          this.showDanger('Se encontro un error en el servidor, comunicate con el Administrador del web site');
        } else {
          this.showDanger('Se encontro un error, comunicate con el Administrador del web site');
        }
        this.loading.hide();
      });
    }
  }

}
