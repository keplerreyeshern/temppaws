import {Component, Input, OnInit} from '@angular/core';
import { Post } from "../../../../interfaces/post";
import { StorageService } from "../../../../services/storage/storage.service";
import { Gallery } from "angular-gallery";
import { environment } from "../../../../../environments/environment";
import { User } from "../../../../interfaces/user";

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.sass']
})
export class PostsComponent implements OnInit {

  @Input() posts: Post[]=[];
  baseUrl = environment.baseUrl;
  user:User=<User>{};

  constructor(public storage: StorageService,
              private gallery: Gallery) { }

  ngOnInit(): void {
    this.user = this.storage.getUser();
  }

  showGallery(index: number, images: any, userId: string) {
    let imagesLoading: any[]=[];
    for (let i=0; images.length>i; i++){
      let image = {path: ''};
      image.path = this.baseUrl + '/posts/image/' + userId + '/' + images[i];
      imagesLoading.push(image);
    }
    let prop = {
      images: imagesLoading,
      index
    };
    this.gallery.load(prop);
  }

}
