import {Component, OnInit, Output, EventEmitter} from '@angular/core';
import { User } from "../../../../interfaces/user";
import { Alert } from "../../../../interfaces/alert";
import { Router } from "@angular/router";
import { NgxSpinnerService } from "ngx-spinner";
import { AuthService } from "../../../../services/sign/auth.service";
import { DataService } from "../../../../services/data/data.service";
import { faFileImage, faImages } from "@fortawesome/free-regular-svg-icons";
import { faTimes } from "@fortawesome/free-solid-svg-icons";
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { PostsService } from "../../../../services/public/posts.service";
import { StorageService } from "../../../../services/storage/storage.service";
import { NavbarService } from "../../../../services/components/navbar.service";

@Component({
  selector: 'app-new-post',
  templateUrl: './new-post.component.html',
  styleUrls: ['./new-post.component.sass']
})
export class NewPostComponent implements OnInit {

  @Output() publishPost = new EventEmitter<string>();

  user:User=<User>{};
  userName: string = '';
  alert:Alert=<Alert>{};
  faFileImage = faFileImage;
  faTimes = faTimes;
  faImages = faImages;
  files: File[] = [];
  filesLength = 0;
  filesLengthCount = 0;
  message:string = '';

  constructor(private router: Router,
              private loading: NgxSpinnerService,
              private storage: StorageService,
              private serviceAuth: AuthService,
              private servicePost: PostsService,
              public componetNavbar: NavbarService,
              private serviceData: DataService,
              private modalService: NgbModal) { }

  ngOnInit(): void {
    this.getData();
  }

  onSelect(event: any) {
    this.files.push(...event.addedFiles);
    console.log(this.files);
  }

  onRemove(event: any) {
    console.log(event);
    this.files.splice(this.files.indexOf(event), 1);
  }

  autoSize(item: any){
    item.style.cssText = 'height:auto; padding:0';
    item.style.cssText = 'height:' + item.scrollHeight + 'px';
  }

  getData(){
    this.loading.show();
    this.user =  this.storage.getUser();
    this.userName = this.user.name.substr(0, this.user.name.indexOf(' '));
    this.loading.hide();
  }

  openPhoto(content: any) {
    this.modalService.open(content, { centered: true });
  }

  closeModal() {
    this.modalService.dismissAll();
  }

  createPostPhoto(){
    this.filesLength = this.files.length;
    if (this.filesLength == 0) {
      this.createPost();
    } else {
      this.loading.show();
      for (let i=0; this.files.length>i;i++){
        let params = new FormData();
        params.append('Content-Type', 'multipart/form-data');
        params.append('image', this.files[i]);
        this.servicePost.postImage(params).subscribe( response => {
          console.log(response);
          if (response.ok){
            this.filesLengthCount = this.filesLengthCount + 1;
            this.createPost();
          } else {
            this.alert = {
              type: 'danger',
              active: true,
              message: response.message
            }
          }
        }, error => {
          if (error.status == 500){
            this.alert = {
              type: 'danger',
              active: true,
              message: 'Se encontro un error en el servidor, comunicate con el Administrador del web site'
            }
          } else {
            this.alert = {
              type: 'danger',
              active: true,
              message: 'Se encontro un error, comunicate con el Administrador del web site'
            }
          }
          this.loading.hide();
          console.log(error);
        });
      }
    }
  }

  createPost(){
    this.loading.show()
    if (this.filesLength == this.filesLengthCount){
      let params = new FormData();
      params.append('Content-Type', 'multipart/form-data');
      params.append('message', this.message);
      this.servicePost.postPost(params).subscribe(response => {
        this.message = '';
        this.publishPost.emit('!El post de ' + this.user.name + ' se publico con exito¡');
        this.loading.hide();
      }, error => {
        if (error.status == 500){
          this.alert = {
            type: 'danger',
            active: true,
            message: 'Se encontro un error en el servidor, comunicate con el Administrador del web site'
          }
        } else {
          this.alert = {
            type: 'danger',
            active: true,
            message: 'Se encontro un error, comunicate con el Administrador del web site'
          }
        }
        this.loading.hide();
      });
      this.closeModal();
    }
  }

}
