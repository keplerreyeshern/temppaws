import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SharedOptionsComponent } from './shared-options.component';

describe('SharedOptionsComponent', () => {
  let component: SharedOptionsComponent;
  let fixture: ComponentFixture<SharedOptionsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SharedOptionsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SharedOptionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
