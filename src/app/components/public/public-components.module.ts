import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";
import { FontAwesomeModule } from "@fortawesome/angular-fontawesome";
import { NgxDropzoneModule } from "ngx-dropzone";
import { NgbModule} from "@ng-bootstrap/ng-bootstrap";

import { BreadcrumbComponent } from "./breadcrumb/breadcrumb.component";
import { NavbarComponent } from './navbar/navbar.component';
import { CommentsComponent } from "./main/comments/comments.component";
import { NewPostComponent } from "./main/new-post/new-post.component";
import { PostsComponent } from "./main/posts/posts.component";
import { SharedOptionsComponent } from "./main/shared-options/shared-options.component";
import { SideBarLeftComponent } from "./side-bar-left/side-bar-left.component";
import { SideBarRightComponent } from "./side-bar-right/side-bar-right.component";
import { ToastsComponent } from "./main/toasts/toasts.component";
import { PipesModule } from "../../pipes/pipes.module";
import { PetsComponent } from './main/pets/pets.component';
import { SharedOptionsPetComponent } from "./main/shared-options-pet/shared-options-pet.component";
import { CommentsPetComponent } from "./main/comments-pet/comments-pet.component";
import { NewPetComponent } from './main/new-pet/new-pet.component';



@NgModule({
  declarations: [
    BreadcrumbComponent,
    CommentsComponent,
    NavbarComponent,
    NewPostComponent,
    PostsComponent,
    SharedOptionsComponent,
    SideBarLeftComponent,
    SideBarRightComponent,
    ToastsComponent,
    PetsComponent,
    SharedOptionsPetComponent,
    CommentsPetComponent,
    NewPetComponent
  ],
  exports: [
    BreadcrumbComponent,
    CommentsComponent,
    NavbarComponent,
    NewPostComponent,
    PostsComponent,
    SharedOptionsComponent,
    SideBarLeftComponent,
    SideBarRightComponent,
    ToastsComponent,
    PetsComponent,
    SharedOptionsPetComponent,
    CommentsPetComponent,
    NewPetComponent
  ],
  imports: [
    CommonModule,
    FontAwesomeModule,
    RouterModule,
    NgbModule,
    NgxDropzoneModule,
    FormsModule,
    PipesModule
  ]
})
export class PublicComponentsModule { }
