import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ImagePipe } from './image.pipe';
import { DomSanitizerPipe } from './dom-sanitizer.pipe';



@NgModule({
  declarations: [
    DomSanitizerPipe,
    ImagePipe
  ],
  exports: [
    DomSanitizerPipe,
    ImagePipe
  ],
  imports: [
    CommonModule
  ]
})
export class PipesModule { }
